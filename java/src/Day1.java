import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

public class Day1 implements Solution {

    class State {
        Bearing bearing = Bearing.N;
        Position position = new Position(0, 0);

        void move(String instruction) {
            instruction = instruction.trim().toUpperCase();

            Turn t = Turn.fromChar(instruction.charAt(0));
            bearing = bearing.turn(t);

            int distance = Integer.parseInt(instruction.substring(1));
            position = position.move(bearing, distance);
        }
    }


    private Pattern delimiter = Pattern.compile(",\\s*");



    public String solve() {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter(delimiter);

        State s = new State();

        Set<Position> visited = new HashSet<>();
        visited.add(s.position);

        while (scanner.hasNext()) {
            s.move(scanner.next());

            if (visited.contains(s.position)) {
                break;
            }

            visited.add(s.position);
        }

        scanner.close();

        return Integer.toString(s.position.distanceFromZero());
    }
}
