import java.util.HashMap;
import java.util.Map;

public class Main {

    private static Map<String, Solution> solutions;
    static {
        solutions = new HashMap<>();
        solutions.put("d1", new Day1());
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            String puzzle = args[0].toLowerCase();
            Solution solver = solutions.getOrDefault(puzzle, () -> {
                return String.format("!! stNo solver for puzzle '%s' !!", puzzle);
            });
            System.out.println(solver.solve());
        }
        else {
            System.out.println("Dude, missing a command line argument!");
        }
    }
}

interface Solution {
    public String solve();
}
