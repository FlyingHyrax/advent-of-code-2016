import java.util.Objects;

class Position {
    final int X;
    final int Y;

    Position(int x, int y) {
        X = x;
        Y = y;
    }

    Position move(Bearing b, int distance) {
        int newX = this.X;
        int newY = this.Y;
        switch (b) {
            case N:
                newY += distance;
                break;
            case E:
                newX += distance;
                break;
            case S:
                newY -= distance;
                break;
            case W:
                newX -= distance;
                break;
        }
        return new Position(newX, newY);
    }

    int distanceFromZero() {
        return Math.abs(this.X) + Math.abs(this.Y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return X == position.X &&
                Y == position.Y;
    }

    @Override
    public int hashCode() {
        return X + 31 * Y;
    }

    @Override
    public String toString() {
        return "Position{" +
                "X=" + X +
                ", Y=" + Y +
                '}';
    }
}
