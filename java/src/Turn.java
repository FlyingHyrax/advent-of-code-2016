public enum Turn {
    L, R;

    public static Turn fromChar(char c) {
        if (c == 'l' || c == 'L') {
            return Turn.L;
        }
        else if (c == 'r' || c == 'R') {
            return Turn.R;
        }
        else {
            throw new IllegalArgumentException(String.format("'%c' is neither 'R' nor 'L'", c));
        }
    }

    @Override
    public java.lang.String toString() {
        switch (this) {
            case L:
                return "Left";
            case R:
            default:
                return "Right";
        }
    }
}
