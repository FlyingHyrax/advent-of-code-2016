public enum Bearing {
    N, S, E, W;

    public Bearing turn(Turn t) {
        switch (this) {
            case N:
                return t == Turn.L ? Bearing.W : Bearing.E;
            case E:
                return t == Turn.L ? Bearing.N : Bearing.S;
            case S:
                return t == Turn.L ? Bearing.E : Bearing.W;
            case W:
            default:
                return t == Turn.L ? Bearing.S : Bearing.N;
        }
    }
}

